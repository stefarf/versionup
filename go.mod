module gitlab.com/stefarf/versionup

go 1.19

require (
	gitlab.com/stefarf/go v0.0.0-20210902190446-1403c9d88a0f
	gopkg.in/yaml.v3 v3.0.1
)
