package main

import (
	"log"
	"os"
	"regexp"
	"strings"
)

const repoName = "target"

var reVersion = regexp.MustCompile(`(\d+(\.\d+(\.\d+)?)?)`)

type Config struct {
	VersionFile      string     `yaml:"version_file"`
	VersionUpCommand RunCommand `yaml:"version_up_command"`
}

func versionUp(repoDir string, cfg Config) error {
	err := gitCloneIfNotExists(repoDir)
	if err != nil {
		return err
	}
	err = gitForceCheckout(repoDir)
	if err != nil {
		return err
	}
	err = runVersionUpCommand(cfg.VersionUpCommand, repoDir)
	if err != nil {
		return err
	}
	err = gitPush(repoDir, cfg.VersionFile)
	if err != nil {
		return err
	}
	return nil
}

func createRepoBaseDirIfNotExists() error {
	notExists, err := dirNotExists(*fRepoBaseDir)
	if err != nil {
		return err
	}
	if notExists {
		log.Println("Creating repo base dir:", *fRepoBaseDir)
		err := os.MkdirAll(*fRepoBaseDir, 0770)
		if err != nil {
			return err
		}
	}
	return nil
}

func gitCloneIfNotExists(repoDir string) error {
	notExists, err := dirNotExists(repoDir)
	if err != nil {
		return err
	}
	if notExists {
		log.Println("Cloning git:", repoName)
		err := runCommand(RunCommand{
			Dir:  *fRepoBaseDir,
			Cmd:  "git",
			Args: []string{"clone", *fRepoUrl, repoDir},
			Env:  os.Environ(),
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func gitForceCheckout(repoDir string) error {
	err := runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"config", "pull.rebase", "false"},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	err = runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"fetch", "--all"},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	err = runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"reset", "--hard", "origin/" + *fBranch},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	return nil
}

func runVersionUpCommand(rc RunCommand, repoDir string) error {
	return runCommand(RunCommand{
		Dir:  strings.ReplaceAll(rc.Dir, "${REPO_DIR}", repoDir),
		Cmd:  rc.Cmd,
		Args: rc.Args,
		Env:  append(os.Environ(), rc.Env...),
	})
}

func gitPush(repoDir, versionFile string) error {
	b, err := os.ReadFile(versionFile)
	if err != nil {
		return err
	}
	version := reVersion.FindString(string(b))
	log.Printf("Version: %s", version)

	err = runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"add", versionFile},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	err = runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"config", "--global", "user.email", *fUserEmail},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	err = runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"config", "--global", "user.name", *fUserName},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	err = runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"commit", "-m", "Version: " + version},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	err = runCommand(RunCommand{
		Dir:  repoDir,
		Cmd:  "git",
		Args: []string{"push", "origin", "HEAD:" + *fBranch},
		Env:  os.Environ(),
	})
	if err != nil {
		return err
	}
	return nil
}
