package main

import (
	"errors"
	"fmt"
	"os"
)

func dirNotExists(dir string) (bool, error) {
	info, err := os.Stat(dir)
	if err == nil {
		if !info.IsDir() {
			return true, fmt.Errorf("%s is not a directory", dir)
		}
		return false, nil
	} else if errors.Is(err, os.ErrNotExist) {
		return true, nil
	} else {
		return true, err
	}
}
