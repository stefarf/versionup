package main

import (
	"os"
	"os/exec"
)

type RunCommand struct {
	Dir  string
	Cmd  string
	Args []string
	Env  []string
}

func runCommand(r RunCommand) error {
	cmd := exec.Command(r.Cmd, r.Args...)
	cmd.Env = r.Env
	cmd.Dir = r.Dir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}
