package main

import (
	"flag"
	"log"
	"strings"
)

var (
	fUserName    = flag.String("userName", "Arief", "git user name")
	fUserEmail   = flag.String("userEmail", "arief@vostra.co.id", "git user email")
	fRepoBaseDir = flag.String("repoBaseDir", "/tmp/repos", "repository base directory")
	fConfigFile  = flag.String("cfg", "versionup.yaml", "config file")

	fRepoUrl = flag.String("repoUrl", "", "repository URL")
	fBranch  = flag.String("branch", "", "branch name")
)

func init() {
	flag.Parse()
	var errors []string
	if *fRepoUrl == "" {
		errors = append(errors, "Undefined -repoUrl")
	}
	if *fBranch == "" {
		errors = append(errors, "Undefined -branch")
	}
	if errors != nil {
		log.Fatal(strings.Join(errors, "\n"))
	}
}
