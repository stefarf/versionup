package main

import (
	"os"
	"path"
	"strings"

	"gitlab.com/stefarf/go/iferr"
	"gopkg.in/yaml.v3"
)

func main() {
	repoDir := path.Join(*fRepoBaseDir, repoName)
	cfg, err := readConfig(repoDir)
	iferr.Exit(err, "Read configuration")

	iferr.Exit(createRepoBaseDirIfNotExists(), "Create repo base dir")
	iferr.Exit(versionUp(repoDir, cfg), "Version up")
}

func readConfig(repoDir string) (Config, error) {
	var repoConfig Config
	b, err := os.ReadFile(*fConfigFile)
	if err != nil {
		return Config{}, err
	}
	err = yaml.Unmarshal(b, &repoConfig)
	if err != nil {
		return Config{}, err
	}

	// Replace the ${REPO_DIR}
	var cmdEnv []string
	for _, env := range repoConfig.VersionUpCommand.Env {
		cmdEnv = append(cmdEnv, strings.ReplaceAll(env, "${REPO_DIR}", repoDir))
	}
	repoConfig.VersionUpCommand.Env = cmdEnv
	repoConfig.VersionFile = strings.ReplaceAll(repoConfig.VersionFile, "${REPO_DIR}", repoDir)

	return repoConfig, nil
}
